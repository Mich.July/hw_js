// Якщо змінна a дорівнює 10, то виведіть 'Вірно', інакше виведіть 'Неправильно'

let num = parseInt (prompt('Яке число?'));
let res = num === 10 ? 'Вірно!' : 'Неправильно!';
alert (res);


// У змінній min лежить число від 0 до 59.
//  Визначте, в яку чверть години потрапляє це число (У першу, другу, третю або четверту).

let numInput = parseInt(prompt('В якій чверті?'));

if (numInput >= 0 && numInput < 15) {
  alert('Перва чверть');
}else if (numInput < 30) {
  alert ('Друга чверть');
}else if (numInput < 45) {
  alert ('Третя чверть');
}else if (numInput < 59) {
  alert ('Четверта чверть');
}else {
  alert ('Невірні дані. Введіть число в проміжку від 0 до 59')
}


// Змінна num може приймати 4 значення: 1, 2, 3 або 4. Якщо вона має значення '1', то змінну result запишемо 'зима', 
// якщо має значення '2' – 'весна' тощо. Розв'яжіть завдання через switch-case

let season = prompt ('Enter number of season');

switch (season) {
  case '1': {
    alert('Summer');
    break;
  }
  case '2': {
    alert('Autumn');
    break;
  }
  case '3': {
    alert('Winter');
    break;
  }
  case '4': {
    alert('Spring');
    break;
  }
  default: {
    alert ('Введіть правильні числа(1, 2, 3 або 4)')
  }
}


// Використовуючи цикли та умовні конструкції намалюйте ялинку

document.write ('<div style = "text-align: center; width: 20%; line-height: 0.4;">')

for ( let i = 0; i < 10; i++ ) {
  for ( let j = i; j > 0; j-- ) {
    document.write (' * ')
  }
  document.write ('<br>')
}
for ( let i = 0; i < 14; i++ ) {
  for ( let j = i; j > 0; j-- ) {
    document.write (' * ')
  }
  document.write ('<br>')
}
for ( let i = 0; i < 18; i++ ) {
  for ( let j = i; j > 0; j-- ) {
    document.write (' * ')
  }
  document.write ('<br>')
}



//  Використовуючи умовні конструкції перевірте вік користувача, якщо користувачеві буде від 18 до 35 років переведіть його на сайт google.com
//  через 2 секунди, якщо вік користувача буде від 35 до 60 років, переведіть його на сайт https://www.uz.gov.ua/,
//  якщо користувачеві буде до 18 років покажіть йому першу серію лунтика з ютубу Виконайте це завдання за допомогою if, switch, тернарний оператор

let age = prompt('Вкажіть ваш вік');
if (age >= 18 && age < 35) {
  document.write ('<meta http-equiv="refresh" content="2, url=https://www.google.com">');
}else if (age >= 35 && age <=60) {
  document.write ('<meta http-equiv="refresh" content="2, url=https://www.uz.gov.ua">');
}else if (age <= 18 && age >= 0) {
  document.write ('<meta http-equiv="refresh" content="2, url=https://www.youtube.com/watch?v=3Hz3kHRRISU">');
}else {
  document.write ('Не можливо');
};
