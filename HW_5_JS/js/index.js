let newDocument = {
  title: "",
  body: "",
  footer: "",
  date: "",
  vlog: {
    title: {text: ""},
    body: {text: ""},
    footer: {text: ""},
    date: {text: ""},
  },
  

inputDoc: function () {
  this.title = prompt ('Введіть заголовок', 'Заголовок');
  this.body = prompt ("Введіть інформацію про об'єкт", "Інформація про об'єкт") ;
  this.footer = prompt ('Введіть контакти', 'Контакти');
  this.date = prompt ('Введіть дату', '22.22.2022');
  },

inputVlog: function () {
  this.vlog.title.text = prompt ('Введіть заголовок додатку', 'Заголовок додатку');
  this.vlog.body.text = prompt ("Введіть інформацію про додаток об'єкту", "Інформація про додаток") ;
  this.vlog.footer.text = prompt ('Введіть адресу', 'Адреса');
  this.vlog.date.text = prompt ('Введіть дату', '20.06.2022');
  },

outputDoc: function () {
  document.write(`Заголовок документу - '${this.title}'`);
  document.write(`<br> Тіло документа - '${this.body}'`);
  document.write(`<br> Кінець документа - '${this.footer}'`);
  document.write(`<br> Дата документа - '${this.date}'`);
  },

outputVlog: function () {
  document.write("<br>");
  document.write(`<br> Назва програми - "${this.vlog.title.text}"`);
  document.write(`<br> Тіло програми - "${this.vlog.body.text}"`);
  document.write(`<br> Кінець документа - "${this.vlog.footer.text}"`);
  document.write(`<br> Дата програми - "${this.vlog.date.text}"`);
  },
} 

newDocument.inputDoc ();
newDocument.inputVlog ();

newDocument.outputDoc ();
newDocument.outputVlog ();

