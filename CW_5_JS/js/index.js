const wallet = {
  name: "Misha",
  btc: {
    name: "Bitcoin",
    logo: "<img src ='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png'>",
    rate: "20061.00",
    coin: "25"
  },
  eth: {
    name: "Ethereum",
    logo: "<img src ='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png'>",
    rate: "1109.85",
    coin: "62"
  },
  ste: {
    name: "Stellar",
    logo: "<img src ='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png'>",
    rate: "3.35",
    coin: "7"
  },
  show: function (nameCoin) {
    document.getElementById("wallet").innerHTML = `Добрий день ${wallet.name}! На вашому балансі 
    ${wallet[nameCoin].name} ${wallet[nameCoin].logo} залишилось ${wallet[nameCoin].coin} монет. Якщо ви сьогодні продасте, то отримаєте 
    ${(wallet[nameCoin].rate*wallet[nameCoin].coin*31).toFixed(2)}грн`
  }
}


wallet.show(prompt("Enter the currency", "btc, eth or ste"));
